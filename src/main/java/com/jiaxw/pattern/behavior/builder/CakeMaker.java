package com.jiaxw.pattern.behavior.builder;

import lombok.Data;

/**
 * 蛋糕师 == 根据builder定义的制作工序制作蛋糕
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
@Data
public class CakeMaker implements CakeBuilder {

    /**
     * 蛋糕师傅的姓名
     */
    private String name;

    /**
     * 蛋糕师要制作的蛋糕，蛋糕师不关心蛋糕最后去了哪，他只需要把做好的蛋糕放在货架上即可
     */
    private Cake cake = new Cake();

    public CakeMaker() {

    }

    public CakeMaker(String name) {
        this.name = name;
    }

    /**
     * @param cake the cake to set
     */
    public void setCake(Cake cake) {
        this.cake = cake;
    }

    @Override
    public void knead() {
        cake.addProcess("揉面 -- 软软的，滑滑的");
    }

    @Override
    public void ferment() {
        cake.addProcess("发酵 -- 酵一酵");
    }

    @Override
    public void bake(int minutes) {
        cake.addProcess("烘烤 -- " + minutes + "分钟");
    }

    @Override
    public Cake getCake() {
        return this.cake;
    }

}
