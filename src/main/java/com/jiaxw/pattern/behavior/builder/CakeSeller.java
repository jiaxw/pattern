package com.jiaxw.pattern.behavior.builder;

/**
 * 蛋糕销售者 == 告诉蛋糕师，货架上没蛋糕了，赶紧给我做一个出来，客户等着要呢
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class CakeSeller {

    public CakeSeller() {

    }

    public Cake sell(CakeBuilder cakeBuilder) {
        cakeBuilder.knead();
        cakeBuilder.ferment();
        cakeBuilder.bake(30);
        return cakeBuilder.getCake();
    }

}
