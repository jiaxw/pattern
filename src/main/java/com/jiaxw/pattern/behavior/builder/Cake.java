package com.jiaxw.pattern.behavior.builder;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 蛋糕类
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
@Data
public class Cake {

    /**
     * 制作一个蛋糕所需要的所有工序【或一个产品所有组成部分的实现】
     */
    private List<String> parts = new ArrayList<>();

    public void addProcess(String process) {
        this.parts.add(process);
    }

    public void show() {
        System.out.println("先生你好，香喷喷的蛋糕已为你准备好了，它的制作流程如下：");
        for (String part : parts) {
            System.out.println(part);
        }
    }

}
