package com.jiaxw.pattern.behavior.prototype;

import lombok.Data;

import java.io.Serializable;

/**
 * 小学生 == 如果要实现对象的深度拷贝，嵌套引用类型的对象类必须也实现Serializable接口
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
@Data
public class PupilStudent implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 学号
     */
    private Long sNo;

    /**
     * 年级
     */
    private Integer sClass;

    /**
     * 姓名
     */
    private String name;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 性别
     */
    private SexEnum sex = SexEnum.MALE;

}
