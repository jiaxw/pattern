package com.jiaxw.pattern.behavior.prototype;

import java.util.Calendar;
import java.util.Date;

/**
 * 原型模式测试 == 两种方式，你选哪一种？
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class PrototypeTest {

    public void cloneTest() throws CloneNotSupportedException {

        // 原型 == 我们创建一个已经由小学生【刘晓然】完成的作业对象
        HomeWork homeWork = new HomeWork();

        // 设置作业信息
        homeWork.setType(WorkTypeEnum.WULI);
        homeWork.setPages(12);
        homeWork.setFinishTime(new Date());

        // 设置小学生信息 == 刘晓然
        PupilStudent pupilStudent = new PupilStudent();
        pupilStudent.setSNo(1001L);
        pupilStudent.setName("刘晓然");
        pupilStudent.setAge(10);
        pupilStudent.setSex(SexEnum.FEMALE);
        pupilStudent.setSClass(4);
        homeWork.setPupilStudent(pupilStudent);

        // 1、原型模式第一种 == 作业对象浅拷贝测试
        HomeWork ykHomeWork = shallowCopy(homeWork);
        System.out.println("刘晓然的作业：\n" + homeWork);
        System.out.println("我的作业：\n" + ykHomeWork);

        System.out.println("=========================================分割线");

        // 2、原型模式第二种 == 作业对象深拷贝测试
        HomeWork zhangHomeWork = deepCopy(ykHomeWork);
        System.out.println("Appleyk的作业：\n" + ykHomeWork);
        System.out.println("张聪明的作业：\n" + zhangHomeWork);
    }

    /**
     * 对象浅拷贝
     *
     * @param homeWork
     * @return
     * @throws CloneNotSupportedException
     */
    public static HomeWork shallowCopy(HomeWork homeWork) throws CloneNotSupportedException {
        HomeWork myHomeWork = homeWork.clone();
        // 开始改造  == 首先改完成时间
        myHomeWork.setFinishTime(addDay(new Date(), 1));
        // 然后改作业的完成者，就是我 == 【Appleyk】
        PupilStudent mySelf = myHomeWork.getPupilStudent();
        // 学号肯定不能一样吧，不然这还不被发现作业是抄的吗
        mySelf.setSNo(1002L);
        // 我去，还要改名字，这事我差点忘了
        mySelf.setName("Appleyk");
        // 性别，对，还有性别，这个不能粗心大意，忘改了
        mySelf.setSex(SexEnum.MALE);
        // OK，一切就绪，改的那叫一个相当顺利啊，哈哈哈哈！ == 满心欢喜交作业咯
        return myHomeWork;
    }

    /**
     * 对象深度拷贝
     *
     * @param homeWork
     * @return
     */
    public static HomeWork deepCopy(HomeWork homeWork) {
        HomeWork myHomeWork = homeWork.deepClone();
        // 开始改造  == 首先改完成时间，完成时间+1天
        myHomeWork.setFinishTime(addDay(new Date(), 1));
        PupilStudent mySelf = myHomeWork.getPupilStudent();
        mySelf.setSNo(1003L);
        mySelf.setName("张聪明");
        mySelf.setSex(SexEnum.MALE);
        return myHomeWork;
    }

    public static Date addDay(Date date, int num) {
        Calendar start = Calendar.getInstance();
        start.setTime(date);
        start.add(Calendar.DATE, num);
        return start.getTime();
    }

}
