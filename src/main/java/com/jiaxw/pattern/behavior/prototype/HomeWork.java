package com.jiaxw.pattern.behavior.prototype;

import lombok.Data;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 小学生的家庭作业
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
@Data
public class HomeWork implements Cloneable, Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 作业类型
     */
    private WorkTypeEnum type = WorkTypeEnum.YUWEN;

    /**
     * 完成时间
     */
    // @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date finishTime;

    /**
     * 作业页码数【作业量】
     */
    private Integer pages = 0;

    /**
     * 完成者
     */
    private PupilStudent pupilStudent;

    public HomeWork() {

    }

    /**
     * 对象浅拷贝  == 对象中按值类型传递部分均能完美拷贝走，但是按引用类型传递部分则拷贝不走
     *
     * @return
     * @throws CloneNotSupportedException
     */
    @Override
    public HomeWork clone() throws CloneNotSupportedException {
        return (HomeWork) super.clone();
    }

    /**
     * 深度拷贝 == 不管你对象中是值类型部分，还是引用类型部分，我全部拿走
     * 对象字节流的序列与反序列化 ==> 对象完全、深度、彻彻底底的Copy！！！
     *
     * @return
     */
    public HomeWork deepClone() {
        // Anything 都是可以用字节流进行表示，记住是任何！
        HomeWork homeWork = null;
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            // 将当前的对象写入 baos 【输出流 -- 字节数组】里
            oos.writeObject(this);
            // 从输出字节数组缓存区中拿到字节流
            byte[] bytes = baos.toByteArray();
            // 创建一个输入字节数组缓冲区
            ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
            // 创建一个对象输入流
            ObjectInputStream ois = new ObjectInputStream(bais);
            // 下面将反序列化字节流 == 重新开辟一块空间存放反序列化后的对象
            homeWork = (HomeWork) ois.readObject();
        } catch (Exception e) {
            System.out.println((e.getClass() + ":" + e.getMessage()));
        }
        return homeWork;
    }

    @Override
    public String toString() {
        return String.format("类型：%s,页数：%s,完成时间：%s," +
                        "完成者：%s,学号：%d,年级：%d,年龄:%d,性别：%s", this.type.getName()
                , this.pages, getFormatString(this.finishTime), this.pupilStudent.getName(),
                this.pupilStudent.getSNo(), this.pupilStudent.getSClass(),
                this.pupilStudent.getAge(), this.pupilStudent.getSex().getName());
    }

    public static String getFormatString(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String str = sdf.format(date);
        return str;
    }
}
