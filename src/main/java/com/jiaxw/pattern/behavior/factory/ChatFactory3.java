package com.jiaxw.pattern.behavior.factory;

/**
 * 静态工厂模式，无需创建工厂类实例
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class ChatFactory3 {

    /**
     * QQ
     *
     * @return
     */
    public static Chat createQQ() {
        return new QQ();
    }

    /**
     * WeChat
     *
     * @return
     */
    public static Chat createWeChat() {
        return new QQ();
    }

}
