package com.jiaxw.pattern.behavior.factory;

import lombok.extern.slf4j.Slf4j;

/**
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
@Slf4j
public class QQ implements Chat {

    @Override
    public void onLineChat(String no) {
        log.info("正在 QQ 上跟 {} 聊天", no);
    }

}
