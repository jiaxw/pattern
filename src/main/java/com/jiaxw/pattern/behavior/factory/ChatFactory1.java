package com.jiaxw.pattern.behavior.factory;

/**
 * 单方法工厂模式
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class ChatFactory1 {

    /**
     * QQ
     */
    public static final String QQ = "QQ";

    /**
     * WeChat
     */
    public static final String WECHAT = "WeChat";

    /**
     * 创建工厂
     *
     * @param tools
     * @return
     */
    public Chat createChat(String tools) {
        if (QQ.equalsIgnoreCase(tools)) {
            return new QQ();
        }

        if (WECHAT.equalsIgnoreCase(tools)) {
            return new WeChat();
        }

        return null;
    }

}
