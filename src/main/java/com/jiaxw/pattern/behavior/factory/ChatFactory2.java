package com.jiaxw.pattern.behavior.factory;

/**
 * 多方法工厂模式，需要哪个，就用哪个方法
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class ChatFactory2 {

    /**
     * QQ
     *
     * @return
     */
    public Chat createQQ() {
        return new QQ();
    }

    /**
     * WeChat
     *
     * @return
     */
    public Chat createWeChat() {
        return new QQ();
    }

}
