package com.jiaxw.pattern.behavior.factory;

/**
 * 工厂模式测试类
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class FactoryTest {


    public static void main(String[] args) {
        chatFactory1Test();
        chatFactory2Test();
        chatFactory3Test();
    }

    /**
     * 坏处，乱传参数，可能导致空指针，代码多
     */
    public static void chatFactory1Test() {

        ChatFactory1 chatFactory1 = new ChatFactory1();

        Chat qq = chatFactory1.createChat(ChatFactory1.QQ);
        qq.onLineChat("710833488");

        Chat weChat = chatFactory1.createChat(ChatFactory1.WECHAT);
        weChat.onLineChat("357002020");

        Chat chat = chatFactory1.createChat(null);
        if (null == chat) {
        } else {
            chat.onLineChat("357002020");
        }
    }

    /**
     * 有效防止创建不存在实例
     */
    public static void chatFactory2Test() {
        ChatFactory2 chatFactory2 = new ChatFactory2();

        Chat qq = chatFactory2.createQQ();
        qq.onLineChat("710833488");

        Chat weChat = chatFactory2.createWeChat();
        weChat.onLineChat("357002020");
    }

    /**
     * 代码更精简，推荐使用
     */
    public static void chatFactory3Test() {

        Chat qq = ChatFactory3.createQQ();
        qq.onLineChat("710833488");

        Chat weChat = ChatFactory3.createWeChat();
        weChat.onLineChat("357002020");
    }

}
