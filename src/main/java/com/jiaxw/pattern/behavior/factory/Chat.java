package com.jiaxw.pattern.behavior.factory;

/**
 * 聊天
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public interface Chat {

    /**
     * 线上聊天
     *
     * @param no
     */
    void onLineChat(String no);

}
