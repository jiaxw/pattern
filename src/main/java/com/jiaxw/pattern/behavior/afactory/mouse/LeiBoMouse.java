package com.jiaxw.pattern.behavior.afactory.mouse;

import com.jiaxw.pattern.behavior.afactory.ainterface.ProduceMouse;

/**
 * 雷柏（Rapoo）鼠标生产商
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class LeiBoMouse implements ProduceMouse {

    @Override
    public void produceMouse(String name, String type) {
        System.out.println("雷柏鼠标 -- " + name + "," + type);
    }

}
