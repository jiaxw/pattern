package com.jiaxw.pattern.behavior.afactory.keyboard;

import com.jiaxw.pattern.behavior.afactory.ainterface.ProduceKeyBord;

/**
 * 雷柏（Rapoo）键盘生产商
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class LeiBoKeyBord implements ProduceKeyBord {

    @Override
    public void produceKeyBord(String name, String color) {
        System.out.println("雷柏键盘 -- " + name + "," + color);
    }

}
