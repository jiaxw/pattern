package com.jiaxw.pattern.behavior.afactory.ainterface;

/**
 * 鼠标接口 -- 生产鼠标
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public interface ProduceMouse {

    /**
     * 约定鼠标生产的规格数据
     *
     * @param name 名称
     * @param type 类型
     */
    void produceMouse(String name, String type);

}
