package com.jiaxw.pattern.behavior.afactory;

import com.jiaxw.pattern.behavior.afactory.ainterface.ProduceKeyBord;
import com.jiaxw.pattern.behavior.afactory.ainterface.ProduceMouse;
import com.jiaxw.pattern.behavior.afactory.factory.LianXiang01Factory;
import com.jiaxw.pattern.behavior.afactory.factory.LianXiang02Factory;

/**
 * 抽象工厂测试 == 具体工厂类实现不同电脑组件的生产
 * 好处：横向扩展很容易，如果我需要再增加一个电脑型号的生产线，比如HP【惠普】，只需要在创建一个对应的工厂实现抽象工厂即可
 * 坏处：纵向扩展很麻烦，如果我需要增加显示器的生产接口，那么改动的地方就太多了，自己领会吧，可以尝试一下
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class AFactoryTest {

    public static void main(String[] args) {
        factory1Test();
        factory2Test();
    }

    public static void factory1Test() {
        // 用户需要购买联想的电脑，但要求键盘和鼠标都是雷柏的，于是便找到了对应的01电脑组装工厂进行生产
        LianXiang01Factory lianXiang01Factory = new LianXiang01Factory();
        ProduceKeyBord keyBord = lianXiang01Factory.createKeyBord();
        keyBord.produceKeyBord("M550", "黑色");
        ProduceMouse mouse = lianXiang01Factory.createMouse();
        mouse.produceMouse("M590", "有线");
    }


    public static void factory2Test() {
        // 用户需要购买联想的电脑，但要求键盘是雷柏的，鼠标是罗技的，于是便找到了对应的02电脑组装工厂进行生产
        LianXiang02Factory lianXiang02Factory = new LianXiang02Factory();
        ProduceKeyBord keyBord = lianXiang02Factory.createKeyBord();
        keyBord.produceKeyBord("M550", "黑色");
        ProduceMouse mouse = lianXiang02Factory.createMouse();
        mouse.produceMouse("M590", "无线");
    }

}
