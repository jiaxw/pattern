package com.jiaxw.pattern.behavior.afactory.mouse;

import com.jiaxw.pattern.behavior.afactory.ainterface.ProduceMouse;

/**
 * <p>罗技（G）鼠标生产商</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class LuoJiMouse implements ProduceMouse {

    @Override
    public void produceMouse(String name, String type) {
        System.out.println("罗技鼠标 -- " + name + "," + type);
    }

}
