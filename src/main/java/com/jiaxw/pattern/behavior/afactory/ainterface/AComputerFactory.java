package com.jiaxw.pattern.behavior.afactory.ainterface;

/**
 * 电脑抽象工厂接口 == 只提供电脑组件的组装，具体组装成什么样型号的电脑，再由具体工厂类决定
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public abstract interface AComputerFactory {

    /**
     * 创建键盘
     *
     * @return
     */
    ProduceKeyBord createKeyBord();

    /**
     * 创建鼠标
     *
     * @return
     */
    ProduceMouse createMouse();

}
