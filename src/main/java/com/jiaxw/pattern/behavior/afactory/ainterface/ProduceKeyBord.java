package com.jiaxw.pattern.behavior.afactory.ainterface;

/**
 * 键盘接口 -- 生产键盘
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public interface ProduceKeyBord {

    /**
     * 约定键盘生产的规格数据
     *
     * @param name  名称
     * @param color 颜色
     */
    void produceKeyBord(String name, String color);

}
