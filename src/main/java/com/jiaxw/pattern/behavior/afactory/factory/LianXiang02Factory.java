package com.jiaxw.pattern.behavior.afactory.factory;

import com.jiaxw.pattern.behavior.afactory.ainterface.AComputerFactory;
import com.jiaxw.pattern.behavior.afactory.ainterface.ProduceKeyBord;
import com.jiaxw.pattern.behavior.afactory.ainterface.ProduceMouse;
import com.jiaxw.pattern.behavior.afactory.keyboard.LeiBoKeyBord;
import com.jiaxw.pattern.behavior.afactory.mouse.LuoJiMouse;

/**
 * 联想电脑生产商 == 电脑型号：02【固定键盘和鼠标生产商】
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class LianXiang02Factory implements AComputerFactory {

    @Override
    public ProduceKeyBord createKeyBord() {
        // 使用雷柏的键盘
        return new LeiBoKeyBord();
    }

    @Override
    public ProduceMouse createMouse() {
        // 使用罗技的鼠标
        return new LuoJiMouse();
    }

}
