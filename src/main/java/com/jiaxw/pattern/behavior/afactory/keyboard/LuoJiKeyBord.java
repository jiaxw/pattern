package com.jiaxw.pattern.behavior.afactory.keyboard;

import com.jiaxw.pattern.behavior.afactory.ainterface.ProduceKeyBord;

/**
 * 罗技（G）键盘生产商
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class LuoJiKeyBord implements ProduceKeyBord {

    @Override
    public void produceKeyBord(String name, String color) {
        System.out.println("罗技键盘 -- " + name + "," + color);
    }

}
