package com.jiaxw.pattern.behavior.singleton;

/**
 * 单例测试类
 * 以上四种单例模式，没有一种是十全十美的
 * 就拿最后一种来说，通过内部类机制达到了线程安全且实例只被创建一次，这些都是JVM帮我们实现的，我们无需关心
 * 但是，如果内部类自己的构造函数出现了问题，而不是在创建instance实例的时候出现了问题
 * 这时候，我们试想一下，我们外部还能顺利拿到单例对象吗？答案是肯定不行
 * 虽然这无疑会增加一道风险，但是相比我们手动加同步块来说，要好的多了
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class SingletonTest {

    public static void main(String[] args) {
        singleton1Test();
        singleton2Test();
        singleton3Test();
        singleton4Test();
    }

    public static void singleton1Test() {
        Singleton1 instance = Singleton1.getInstance();
        instance.show();
    }

    public static void singleton2Test() {
        Singleton2 instance = Singleton2.getInstance();
        instance.show();
    }

    public static void singleton3Test() {
        Singleton3 instance = Singleton3.getInstance();
        instance.show();
    }

    public static void singleton4Test() {
        Singleton4 instance = Singleton4.getInstance();
        instance.show();
    }

}
