package com.jiaxw.pattern.behavior.singleton;

/**
 * 单例模式 -- 线程安全，只在第一次创建实例的时候加锁
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class Singleton3 {

    /**
     * 私有构造器，防止外部new实例
     */
    private Singleton3() {

    }

    private static Singleton3 instance = null;

    /**
     * 区别于Singleton2,注意synchronized关键字【同步块】的位置
     *
     * @return
     */
    public static Singleton3 getInstance() {
        if (null == instance) {
            synchronized (Singleton3.class) {
                if (null == instance) {
                    instance = new Singleton3();
                }
            }
        }
        return instance;
    }

    public void show() {
        System.out.println("你好，我是单例模式三！");
    }

}
