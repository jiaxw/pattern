package com.jiaxw.pattern.structural.composite;

import java.util.List;

/**
 * <p>登录流程图 == 继承抽象树结构</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class LoginDiagram extends AbstractTree {

    public LoginDiagram(String name) {
        super(name);
    }

    @Override
    public void traverse(TreeNode node) {

        if (node.isRoot()) {
            System.out.println("根：" + node.getName() + ",父节点：" + node.getParent());
        } else if (node.isLeaf()) {
            System.out.println("叶子：" + node.getName() + ",父节点：" + node.getParent().getName());
        } else {
            System.out.println("枝：" + node.getName() + ",父节点：" + node.getParent().getName());
        }

        // 递归遍历
        List<TreeNode> childrens = node.getChildrens();
        for (TreeNode cNode : childrens) {
            traverse(cNode);
        }
    }
}
