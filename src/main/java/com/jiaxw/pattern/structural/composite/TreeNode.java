package com.jiaxw.pattern.structural.composite;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>树节点</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class TreeNode {

    /**
     * 节点名称
     */
    private String name;

    /**
     * 节点的父节点
     */
    private TreeNode parent;

    /**
     * 节点的子节点
     */
    private List<TreeNode> childrens = new ArrayList<>();

    public TreeNode() {
    }

    public TreeNode(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TreeNode getParent() {
        return parent;
    }

    public void setParent(TreeNode parent) {
        this.parent = parent;
    }

    public List<TreeNode> getChildrens() {
        return childrens;
    }

    public void setChildrens(List<TreeNode> childrens) {
        this.childrens = childrens;
    }

    public void addChildren(TreeNode node) {
        // 别忘设置节点的父
        node.setParent(this);
        this.childrens.add(node);
    }

    public TreeNode addChildren(String node) {
        TreeNode cNode = new TreeNode(node);
        // 别忘设置节点的父
        cNode.setParent(this);
        this.childrens.add(cNode);
        return cNode;
    }

    public void removeChildren(TreeNode node) {
        this.childrens.remove(node);
    }

    /**
     * 是否是叶子
     */
    public boolean isLeaf() {
        return !(this.childrens.size() > 0) && (this.parent != null);
    }

    /**
     * 是否有父节点 == 如果没有，就是根节点
     *
     * @return
     */
    public boolean hasParent() {
        return !(this.parent == null);
    }

    /**
     * 根节点没有父节点
     *
     * @return
     */
    public boolean isRoot() {
        return this.parent == null;
    }
}
