package com.jiaxw.pattern.structural.composite;

/**
 * <p>树</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public abstract class AbstractTree {

    private TreeNode root = null;

    public AbstractTree() {
    }

    public TreeNode getRoot() {
        return root;
    }

    public AbstractTree(String name) {
        root = new TreeNode(name);
    }

    /**
     * 抽象方法 == 树节点遍历，由具体的子类实现
     */
    public abstract void traverse(TreeNode node);

}
