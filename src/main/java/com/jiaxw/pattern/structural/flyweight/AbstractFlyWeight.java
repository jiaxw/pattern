package com.jiaxw.pattern.structural.flyweight;

/**
 * <p>享元模式 -- 抽象父类</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public abstract class AbstractFlyWeight {

    /**
     * 模拟不同数据库的连接
     */
    public abstract void connection();

}
