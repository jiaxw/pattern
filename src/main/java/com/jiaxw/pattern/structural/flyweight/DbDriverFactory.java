package com.jiaxw.pattern.structural.flyweight;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * <p>数据源驱动工厂类</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class DbDriverFactory {

    private HashMap<String, MyDbDriver> dbDriverMap;

    private List<MyDbDriver> dbDrivers = new ArrayList<>();

    public DbDriverFactory() {
        dbDriverMap = new HashMap<>();
    }

    public MyDbDriver getDbDriver(String driverName) {

        MyDbDriver dbDriver;
        // 有的话，直接返回驱动对象
        if (dbDriverMap.containsKey(driverName)) {
            System.out.println(driverName + "--> 数据源驱动实例已存在，无需再new，直接返回");
            dbDriver = dbDriverMap.get(driverName);
            this.dbDrivers.add(dbDriver);
            return dbDriver;
        }

        dbDriver = new MyDbDriver(driverName);
        this.dbDriverMap.put(driverName, dbDriver);
        this.dbDrivers.add(dbDriver);
        return dbDriver;
    }

    public int size() {
        return dbDriverMap.size();
    }

    public void showConns() {
        this.dbDrivers.forEach(d -> d.connection());
    }

}
