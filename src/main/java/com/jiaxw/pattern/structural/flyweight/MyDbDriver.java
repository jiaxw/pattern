package com.jiaxw.pattern.structural.flyweight;

/**
 * <p>自定义数据源驱动类</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class MyDbDriver extends AbstractFlyWeight {

    /**
     * 数据源驱动名称 == （对于具体的某个数据源驱动，其参数是固定的，因为无需重复new对象）
     */
    private String driverName;

    public MyDbDriver(String driverName) {
        this.driverName = driverName;
    }

    @Override
    public void connection() {
        System.out.println(driverName + " -- 连接数据库");
    }

}
