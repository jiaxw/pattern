package com.jiaxw.pattern.structural.bridge.database;

/**
 * <p>PostGreSql数据库驱动类</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class PostgreSqlDriver implements Driver {

    @Override
    public void connect() {
        System.out.println("postgresql驱动连接数据库");
    }

}
