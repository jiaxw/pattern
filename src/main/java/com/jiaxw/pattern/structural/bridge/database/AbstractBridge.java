package com.jiaxw.pattern.structural.bridge.database;

/**
 * <p>各驱动之间的桥梁</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public abstract class AbstractBridge {

    /**
     * 聚合和关联驱动接口实例
     */
    private Driver driver;

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    /**
     * 驱动连接
     */
    public abstract void connect();

}
