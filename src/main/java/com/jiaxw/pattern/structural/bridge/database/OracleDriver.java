package com.jiaxw.pattern.structural.bridge.database;

/**
 * <p>Oracle数据库驱动类</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class OracleDriver implements Driver {

    @Override
    public void connect() {
        System.out.println("oracle驱动连接数据库");
    }

}
