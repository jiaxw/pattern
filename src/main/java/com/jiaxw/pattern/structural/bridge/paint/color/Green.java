package com.jiaxw.pattern.structural.bridge.paint.color;

/**
 * <p>绿色</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class Green implements Color {

    @Override
    public void painting(String shape) {
        System.out.println("绘制绿色的" + shape);
    }

}
