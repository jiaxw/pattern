package com.jiaxw.pattern.structural.bridge.paint.shape;


import com.jiaxw.pattern.structural.bridge.paint.color.Color;

/**
 * <p>抽象形状类 -- 聚合与关联Color实现类</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public abstract class AbstractShape {

    private Color color;

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    /**
     * 抽象方法 == 绘制图案
     */
    abstract void draw();

}
