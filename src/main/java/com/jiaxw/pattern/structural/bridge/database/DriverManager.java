package com.jiaxw.pattern.structural.bridge.database;

/**
 * <p>驱动管理器 == 实现不同驱动设备之间的数据库连接</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class DriverManager extends AbstractBridge {

    @Override
    public void connect() {
        getDriver().connect();
    }

}
