package com.jiaxw.pattern.structural.bridge.paint.color;

/**
 * <p>白色</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class White implements Color {

    @Override
    public void painting(String shape) {
        System.out.println("绘制白色的" + shape);
    }

}
