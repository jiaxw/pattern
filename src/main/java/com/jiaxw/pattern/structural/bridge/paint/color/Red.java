package com.jiaxw.pattern.structural.bridge.paint.color;

/**
 * <p>红色</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class Red implements Color {

    @Override
    public void painting(String shape) {
        System.out.println("绘制红色的" + shape);
    }

}
