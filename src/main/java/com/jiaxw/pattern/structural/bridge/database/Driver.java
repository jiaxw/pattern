package com.jiaxw.pattern.structural.bridge.database;

/**
 * <p>数据库驱动接口</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public interface Driver {

    /**
     * 连接
     */
    void connect();

}
