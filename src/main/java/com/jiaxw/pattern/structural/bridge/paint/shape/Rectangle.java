package com.jiaxw.pattern.structural.bridge.paint.shape;

/**
 * <p>矩形、长方形</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class Rectangle extends AbstractShape {

    @Override
    public void draw() {
        getColor().painting("矩形");
    }

}
