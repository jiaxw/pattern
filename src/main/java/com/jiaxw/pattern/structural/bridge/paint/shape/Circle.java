package com.jiaxw.pattern.structural.bridge.paint.shape;

/**
 * <p>圆形</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class Circle extends AbstractShape {

    @Override
    public void draw() {
        getColor().painting("圆形");
    }

}
