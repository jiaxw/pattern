package com.jiaxw.pattern.structural.bridge.paint.color;

/**
 * <p>颜色接口</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public interface Color {

    void painting(String shape);

}
