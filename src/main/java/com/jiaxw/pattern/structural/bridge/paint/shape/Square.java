package com.jiaxw.pattern.structural.bridge.paint.shape;

/**
 * <p>正方形</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class Square extends AbstractShape {

    @Override
    public void draw() {
        getColor().painting("正方形");
    }

}
