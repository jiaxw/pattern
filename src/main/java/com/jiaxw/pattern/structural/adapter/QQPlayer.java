package com.jiaxw.pattern.structural.adapter;

/**
 * 适配器 == QQ播放器，将Mp3播放音乐的功能直接扩展到播放器里，即无需再实现播放器播放音乐的功能了
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class QQPlayer extends Mp3 implements Player {

    @Override
    public void playMVs() {
        System.out.println("QQ播放器实现播放MV功能");
    }

    @Override
    public void playMovies() {
        System.out.println("QQ播放器实现播放电影功能");
    }

}
