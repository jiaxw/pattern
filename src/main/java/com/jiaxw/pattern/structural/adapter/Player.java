package com.jiaxw.pattern.structural.adapter;

/**
 * 播放器接口
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public interface Player {

    /**
     * 播放MV
     */
    void playMVs();

    /**
     * 播放音乐 == 与Mp3播放音乐的功能【方法】名称一致
     */
    void playMusics();

    /**
     * 播放电影
     */
    void playMovies();

}
