package com.jiaxw.pattern.structural.adapter;

/**
 * 播放器抽象类 == 实现播放接口
 * 抽象类中的方法：无需全部实现
 * 接口中的方法    ：必须全部实现
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public abstract class AbstractPlayer implements Player {

    /**
     * 播放MV ==
     */
    @Override
    public void playMVs() {
    }

    /**
     * 播放音乐
     */
    @Override
    public void playMusics() {
    }

    /**
     * 播放电影
     */
    @Override
    public void playMovies() {
    }

    /**
     * 播放器功能展示抽象方法，子类中必须实现
     */
    public abstract void show();

}
