package com.jiaxw.pattern.structural.adapter;

/**
 * 播放器B  == 只具有播放MV的功能
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class BPlayer extends AbstractPlayer {

    /**
     * 重写父类方法
     */
    @Override
    public void playMVs() {
        System.out.println("B实现播放MV的功能");
    }

    @Override
    public void show() {
        System.out.println("=====播放器B功能展示：");
        playMusics();
        playMVs();
        playMovies();
    }

}
