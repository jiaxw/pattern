package com.jiaxw.pattern.structural.facade.moduleb;

/**
 * <p>外观模式B == 聚合子模块B中的子系统C和D，降低客户端操作子模块B的复杂度</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class FacadeB {

    private SubSystemC subSystemC;
    private SubSystemD subSystemD;

    public FacadeB() {
        subSystemC = new SubSystemC();
        subSystemD = new SubSystemD();
    }

    /**
     * 将模块B的功能封装一下，松散客户端与A模块中的各个子系统间的耦合关系
     * 让模块中的各个子系统更容易扩展和维护
     */
    public void work() {
        subSystemC.sayHello();
        subSystemD.working();
    }

}
