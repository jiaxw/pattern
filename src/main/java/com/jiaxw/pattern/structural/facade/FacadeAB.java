package com.jiaxw.pattern.structural.facade;

import com.jiaxw.pattern.structural.facade.modulea.FacadeA;
import com.jiaxw.pattern.structural.facade.moduleb.FacadeB;

/**
 * <p>外观模式AB == 聚合子模块A和子模块B的功能，降低客户端使用模块A和模块B的复杂度</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class FacadeAB {

    private FacadeA facadeA;
    private FacadeB facadeB;

    public FacadeAB() {
        facadeA = new FacadeA();
        facadeB = new FacadeB();
    }

    public void startSystem() {
        facadeA.initialize();
        facadeB.work();
    }

}
