package com.jiaxw.pattern.structural.facade.modulea;

/**
 * <p>模块A下的子系统B</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class SubSystemB {

    public void loadDatas() {
        System.out.println("人工智能系统已经启动，正在加载数据 =====>" + this);
        System.out.println(".........");
        System.out.println("数据已完成加载");
    }

    @Override
    public String toString() {
        return "人工智能系统，模块A，子系统B，主要负责加载数据";
    }

}
