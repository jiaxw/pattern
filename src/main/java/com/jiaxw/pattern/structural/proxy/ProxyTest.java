package com.jiaxw.pattern.structural.proxy;

import com.jiaxw.pattern.structural.proxy.service.CommodityService;
import com.jiaxw.pattern.structural.proxy.service.impl.DogImpl;
import com.jiaxw.pattern.structural.proxy.service.impl.UserImpl;

/**
 * <p>代理模式测试</p>
 * 不使用代理:没有对比就没有伤害，主要和下面使用了代理模式的对象进行比较
 * 静态代理:针对特定对象的访问进行"装饰"，虽和装饰者模式很像，但也只是很像，切记搞混淆
 * 动态代理:区别静态代理，静态代理模式在程序编译时即确定了被代理的对象
 * 而动态代理只有在程序运行时才确定要被代理的对象是谁
 * 动态代理主要应用于框架，即反射技术一般用不到，如果用到了，那多半是用于框架级的项目
 * 典型代表：Spring框架 -- AOP【面向切面编程】
 * 虚拟代理:可延缓被代理对象的创建
 * 优点: 程序启动快
 * 缺点: 因为被代理的实例不是在第一时间创建的，因此在使用的时候，
 * 需要狠小心的判断null值，以防止NullPointException
 * <p>
 * 还有其他代理模式，就不一一列举了
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class ProxyTest {

    /**
     * 不使用代理
     */
    public void noProxy() {
        String uName = "奥利奥饼干";
        UserOwn userOwn = new UserOwn();
        userOwn.getCommodity(uName);
        System.out.println("===========分割线");
    }

    /**
     * 使用静态代理
     */
    public void useStaticProxy() {
        String uName = "奥利奥饼干";
        String dName = "狗粮";
        // 使用静态代理模式，通过UU跑腿服务，用户拿到自己要的薯片
        ProxyFactory.getUserProxy().getCommodity(uName);
        System.out.println("===========分割线");
        // 使用静态代理模式，通过UU跑腿服务，宠物狗拿到自己要的狗粮
        ProxyFactory.getDogProxy().getCommodity(dName);
        System.out.println("===========分割线");
    }

    /**
     * 使用动态代理
     */
    public void useDynamicProxy() {
        String uName = "奥利奥饼干";
        String dName = "狗粮";
        // 使用动态代理模式，通过UU跑腿服务，用户拿到自己要的薯片
        CommodityService userProxy = (CommodityService) (ProxyFactory.getDynProxy(new UserImpl()));
        userProxy.getCommodity(uName);
        System.out.println("===========分割线");

        // 使用动态代理模式，通过UU跑腿服务，宠物狗拿到自己要的狗粮
        CommodityService dogProxy = (CommodityService) (ProxyFactory.getDynProxy(new DogImpl()));
        dogProxy.getCommodity(dName);
        System.out.println("===========分割线");
    }

    /**
     * 使用虚拟代理
     */
    public void useVirtualProxy() {
        int second = 5;
        Secretary secretary = new Secretary();
        secretary.addDeal("合同1");
        secretary.addDeal("合同2");
        secretary.sign();

        // 期望领导什么时候出现
        secretary.initLeader(second);

        secretary.addDeal("合同3");
        secretary.addDeal("合同4");
        secretary.sign();
    }

}
