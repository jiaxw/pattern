package com.jiaxw.pattern.structural.proxy.service.impl;

import com.jiaxw.pattern.structural.proxy.service.CommodityService;

/**
 * 用户实现商品类 == 获取吃的
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class UserImpl implements CommodityService {

    @Override
    public void getCommodity(String name) {
        System.out.println("用户获得商品：" + name);
    }

}
