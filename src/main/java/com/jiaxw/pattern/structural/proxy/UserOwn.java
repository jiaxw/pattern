package com.jiaxw.pattern.structural.proxy;


import com.jiaxw.pattern.structural.proxy.service.CommodityService;

/**
 * <p>用户自己去超市取商品 </p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class UserOwn implements CommodityService {

    @Override
    public void getCommodity(String name) {
        goSuperMarket();
        choose(name);
        pay();
        System.out.println("用户获得商品：" + name);
        goHome();
    }

    private void goSuperMarket() {
        System.out.println("去超市");
    }

    private void choose(String name) {
        System.out.println("选商品: " + name);
    }

    private void pay() {
        System.out.println("付钱");
    }

    private void goHome() {
        System.out.println("买完商品，回家");
    }

}
