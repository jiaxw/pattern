package com.jiaxw.pattern.structural.proxy.service;

/**
 * 商品接口
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public interface CommodityService {

    /**
     * 获取指定商品
     *
     * @param name
     */
    void getCommodity(String name);

}
