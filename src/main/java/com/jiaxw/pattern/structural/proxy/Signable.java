package com.jiaxw.pattern.structural.proxy;

/**
 * <p>签订接口 </p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public interface Signable {

    /**
     * 定义签订方法
     */
    void sign();

}
