package com.jiaxw.pattern.structural.proxy;

import com.jiaxw.pattern.structural.proxy.service.CommodityService;
import com.jiaxw.pattern.structural.proxy.service.impl.DogImpl;

/**
 * 宠物狗代理类 == 静态代理模式
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class DogProxy implements CommodityService {

    private DogImpl dog;

    public DogProxy() {
        // 预先确定代理与被代理者的关系 -- 被代理的对象是宠物狗
        dog = new DogImpl();
    }

    @Override
    public void getCommodity(String name) {
        System.out.println("我是UU跑腿的工作人员，我去超市帮助狗狗取狗粮：" + name);
        dog.getCommodity(name);
        System.out.println("商品已成功交给狗狗，期待狗狗的主人好评");
    }

}
