package com.jiaxw.pattern.structural.proxy;

import com.jiaxw.pattern.structural.proxy.service.CommodityService;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * 代理类工厂
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class ProxyFactory {

    /**
     * 获取一个静态用户代理类对象
     */
    public static CommodityService getUserProxy() {
        return new UserProxy();
    }

    /**
     * 获取一个静态宠物狗的代理类对象
     */
    public static CommodityService getDogProxy() {
        return new DogProxy();
    }

    public static Object getDynProxy(Object target) {
        InvocationHandler handler = new DynamicProxy(target);
        return Proxy.newProxyInstance(target.getClass().getClassLoader(), target.getClass().getInterfaces(), handler);
    }

}
