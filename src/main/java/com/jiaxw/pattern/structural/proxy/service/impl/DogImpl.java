package com.jiaxw.pattern.structural.proxy.service.impl;


import com.jiaxw.pattern.structural.proxy.service.CommodityService;

/**
 * 宠物狗实现商品接口 == 获取狗粮
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class DogImpl implements CommodityService {

    @Override
    public void getCommodity(String name) {
        System.out.println("宠物狗获得商品：" + name);
    }

}
