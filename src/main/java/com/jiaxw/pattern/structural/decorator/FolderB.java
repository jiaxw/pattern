package com.jiaxw.pattern.structural.decorator;

/**
 * 文件夹B
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class FolderB implements Folder {

    @Override
    public void mkDir() {
        System.out.println("在Linux系统中创建文件夹B");
    }

    @Override
    public void rmdir() {
        System.out.println("在Linux系统中删除文件夹B");
    }

}
