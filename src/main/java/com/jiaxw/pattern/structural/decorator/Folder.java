package com.jiaxw.pattern.structural.decorator;

/**
 * 文件夹接口 == 提供两个基础的文件夹操作方法
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public interface Folder {

    /**
     * 创建文件夹
     */
    void mkDir();

    /**
     * 删除文件夹
     */
    void rmdir();

}
