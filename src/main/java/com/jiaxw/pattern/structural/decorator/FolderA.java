package com.jiaxw.pattern.structural.decorator;

/**
 * 文件夹A
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class FolderA implements Folder {

    @Override
    public void mkDir() {
        System.out.println("在Windows系统中创建文件夹A");
    }

    @Override
    public void rmdir() {
        System.out.println("在Windows系统中删除文件夹A");
    }

}
