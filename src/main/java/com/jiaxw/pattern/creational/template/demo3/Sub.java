package com.jiaxw.pattern.creational.template.demo3;

/**
 * <p>减法运算</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class Sub extends AbstractCalculator {

    @Override
    protected double calculate(double numA, double numB) {
        return numA - numB;
    }

}
