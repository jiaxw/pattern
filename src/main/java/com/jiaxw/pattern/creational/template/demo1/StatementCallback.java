package com.jiaxw.pattern.creational.template.demo1;

import java.sql.SQLException;
import java.sql.Statement;

/**
 * <p>数据库操作回调接口</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public interface StatementCallback {

    Object doInStatement(Statement stmt) throws SQLException;

}
