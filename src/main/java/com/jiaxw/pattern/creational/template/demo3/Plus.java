package com.jiaxw.pattern.creational.template.demo3;

/**
 * <p>加法运算</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class Plus extends AbstractCalculator {


    @Override
    protected double calculate(double numA, double numB) {
        return numA + numB;
    }


    /**
     * 重写父类钩子方法
     */
    @Override
    public void hookSay() {
        System.out.println("子类Plus说：接下来，由我来计算结果！");
    }

}
