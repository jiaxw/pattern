package com.jiaxw.pattern.creational.memento.demo2;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>备忘录类</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Memento {

    /**
     * 真正备忘录里面的值
     */
    private String value;

}
