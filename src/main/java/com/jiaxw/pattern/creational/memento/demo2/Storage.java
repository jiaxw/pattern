package com.jiaxw.pattern.creational.memento.demo2;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>存储备忘录的内容，可以理解为，备忘录如果是一本书的话，那存储类就是一个抽屉或者其他</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Storage {

    /**
     * 关联一个备忘录对象，这种模式真的是很常见啊
     * 注意，这里可以是一个列表，也可以是单个对象
     * 是单数还是复数，具体看storage的业务需要
     */
    private Memento memento;

}
