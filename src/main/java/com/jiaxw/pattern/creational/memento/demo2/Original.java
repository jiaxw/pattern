package com.jiaxw.pattern.creational.memento.demo2;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>原始类</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Original {

    /**
     * 备忘录的值
     */
    private String value;

    /**
     * <p>创建一个备忘录</p>
     *
     * @return 备忘录对象
     */
    public Memento createMemento() {

        /**
         * 每次都创建一个备忘录对象，并将旧值当参数给它
         * 也就是新进来的值，影响不到原来的备忘录对象
         */
        return new Memento(value);
    }

    /**
     * <p>恢复备忘录的值</p>
     *
     * @param memento 备忘录对象
     */
    public void restoreMemento(Memento memento) {
        this.value = memento.getValue();
    }

}
