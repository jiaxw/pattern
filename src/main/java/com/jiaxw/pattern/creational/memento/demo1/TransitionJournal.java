package com.jiaxw.pattern.creational.memento.demo1;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>事务日志类</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
@Data
public class TransitionJournal {

    /**
     * 日志序号
     */
    private Integer logNum;

    /**
     * 假设数据是存进数据库的
     */
    private List<Integer> data = new ArrayList<>();

    public TransitionJournal(Integer logNum, List<Integer> data) {
        this.logNum = logNum;
        if (data != null && data.size() > 0) {
            this.data.addAll(data);
        }
    }

}
