package com.jiaxw.pattern.creational.strategy.demo1;

/**
 * <p>计算辅助类，主要提取公式中的数数组</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class CalculatorHelper {

    public static double[] getValArray(String formula, String splitChar) {
        // 记得消除空格
        String[] array = formula.trim().split(splitChar);
        double[] arrayDouble = new double[2];
        arrayDouble[0] = Double.parseDouble(array[0]);
        arrayDouble[1] = Double.parseDouble(array[1]);
        return arrayDouble;
    }

}
