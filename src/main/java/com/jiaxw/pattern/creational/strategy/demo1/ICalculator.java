package com.jiaxw.pattern.creational.strategy.demo1;

/**
 * <p>定义计算接口，提供根据公式（字符串）计算值的方法</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public interface ICalculator {

    /**
     * <p>根据公式字符串，计算得出值</p>
     *
     * @param formula 公式
     * @return 值
     */
    double calculate(String formula);

}
