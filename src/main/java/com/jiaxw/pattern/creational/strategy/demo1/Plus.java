package com.jiaxw.pattern.creational.strategy.demo1;

/**
 * <p>加法运算实现加法公式的结果计算</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class Plus implements ICalculator {

    @Override
    public double calculate(String formula) {
        double[] valArray = CalculatorHelper.getValArray(formula, "\\+");
        return valArray[0] + valArray[1];
    }

}
