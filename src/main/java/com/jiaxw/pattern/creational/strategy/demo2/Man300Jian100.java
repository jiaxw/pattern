package com.jiaxw.pattern.creational.strategy.demo2;

/**
 * <p>商品满减活动 -- 满300元减100元</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class Man300Jian100 implements ICalculatePrice {

    @Override
    public double getDiscountedPrice(double totalPrice) {
        // 全场消费满300元，减100元
        if (totalPrice >= 300) {
            return totalPrice - 100;
        }
        return totalPrice;
    }

}
