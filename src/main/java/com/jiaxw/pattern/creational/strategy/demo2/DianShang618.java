package com.jiaxw.pattern.creational.strategy.demo2;

/**
 * <p>电商节 - 6.18 -- 满618元减99，满1000元打8.5折</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class DianShang618 implements ICalculatePrice {

    @Override
    public double getDiscountedPrice(double totalPrice) {
        if (totalPrice >= 618) {
            return totalPrice - 99;
        }
        if (totalPrice >= 1000) {
            return totalPrice * 0.85;
        }
        return totalPrice;
    }

}
