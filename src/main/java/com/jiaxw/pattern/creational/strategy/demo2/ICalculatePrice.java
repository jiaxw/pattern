package com.jiaxw.pattern.creational.strategy.demo2;

/**
 * <p>定义价格计算策略接口，为每一个策略类提供统一的计算折扣价钱的方法</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public interface ICalculatePrice {

    /**
     * <p>根据商品的实际总价计算得到商品折后的价钱</p>
     *
     * @param totalPrice 实际商品总价
     * @return 折后价
     */
    double getDiscountedPrice(double totalPrice);

}
