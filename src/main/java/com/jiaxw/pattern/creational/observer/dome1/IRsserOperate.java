package com.jiaxw.pattern.creational.observer.dome1;

/**
 * <p>订阅者操作接口</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public interface IRsserOperate {

    /**
     * <p>增加一个订阅者</p>
     *
     * @param rss 被增加的订阅者
     */
    void add(IRss rss);

    /**
     * <p>移除一个订阅者</p>
     *
     * @param rss 被移除订阅者
     */
    void del(IRss rss);

    /**
     * <p>根据订阅者的ID移除一个订阅者</p>
     *
     * @param rssID 被移除的订阅者的ID
     */
    void remove(Long rssID);

    /**
     * <p>通知所有订阅者，订阅的内容是什么</p>
     */
    void notifyRssers(String content);

    /**
     * <p>发布消息</p>
     */
    void publish();

}
