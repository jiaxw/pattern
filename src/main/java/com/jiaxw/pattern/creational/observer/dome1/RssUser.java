package com.jiaxw.pattern.creational.observer.dome1;

/**
 * <p></p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class RssUser implements IRss {

    /**
     * 用户ID
     */
    private Long id;

    /**
     * 用户名称
     */
    private String name;

    public RssUser(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public void rss(String content) {
        System.out.println("尊敬的用户（ id = " + this.id + ",name = " + this.name + "),你搜到了一条订阅消息：" + content);
    }

    public Long getId() {
        return id;
    }

}
