package com.jiaxw.pattern.creational.observer.demo2;

/**
 * <p>观察者A</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class ObserverB implements IObserver {

    @Override
    public void discover() {
        System.out.println("观察者B：不好，发现敌情，敌军还有两秒抵达战场！");
    }
}
