package com.jiaxw.pattern.creational.observer.dome1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * <p>抽象一个订阅类，实现rss操作</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public abstract class AbstractSubscription implements IRsserOperate {

    /**
     * 存放订阅了消息的用户
     */
    private List<IRss> rsses = new ArrayList<>();

    @Override
    public void add(IRss rss) {
        this.rsses.add(rss);
    }

    @Override
    public void del(IRss rss) {
        this.rsses.remove(rss);
    }

    @Override
    public void remove(Long rssId) {
        Iterator<IRss> iterator = rsses.iterator();
        while (iterator.hasNext()) {
            RssUser next = (RssUser) iterator.next();
            if (rssId.equals(next.getId())) {
                iterator.remove();
                break;
            }
        }
    }

    @Override
    public void notifyRssers(String content) {
        for (IRss rss : rsses) {
            rss.rss(content);
        }
    }
}
