package com.jiaxw.pattern.creational.observer.demo2;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>抽象一个主类，实现通知接口，并保留扩展方法</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public abstract class AbstractSubject implements INotify {

    private List<IObserver> observers = new ArrayList<>();

    @Override
    public void add(IObserver observer) {
        observers.add(observer);
    }

    @Override
    public void remove(IObserver observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (IObserver observer : observers) {
            observer.discover();
        }
    }
}
