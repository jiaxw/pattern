package com.jiaxw.pattern.creational.observer.demo2;

/**
 * <p>观察者A</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class ObserverA implements IObserver {

    @Override
    public void discover() {
        System.out.println("观察者A：不好，发现敌情，敌军还有三秒抵达战场！");
    }
}
