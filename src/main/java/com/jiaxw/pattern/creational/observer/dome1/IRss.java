package com.jiaxw.pattern.creational.observer.dome1;

/**
 * <p>消息订阅接口，提供一个订阅消息内容的实现</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public interface IRss {

    /**
     * <p>消息订阅，主要输出订阅的内容</p>
     */
    void rss(String content);

}
