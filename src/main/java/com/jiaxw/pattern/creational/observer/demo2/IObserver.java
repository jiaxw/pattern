package com.jiaxw.pattern.creational.observer.demo2;

/**
 * <p>观察者接口，就提供了一个discover方法</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public interface IObserver {

    /**
     * <p>发现，具体发现什么，做什么，交由实现的类吧</p>
     */
    void discover();

}
