package com.jiaxw.pattern.creational.observer.demo2;

/**
 * <p>自定义子类，继承抽象主类，只需要干一件事情即可，通知所有观察者之前，先...</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class MySubject extends AbstractSubject {

    @Override
    public void extend() {
        System.out.println("大事不妙，有敌情！");
        notifyObservers();
    }
}
