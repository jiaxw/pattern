package com.jiaxw.pattern.creational.command;

/**
 * <p>调用者，命令发起者</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class Invoker {

    /**
     * 发号施令的人名字或者下达命令的人
     */
    private String name;

    /**
     * 要求命令对象执行请求，通常会持有命令对象，可以持有很多的命令对象
     */
    private ICommand command;

    public Invoker(ICommand command) {
        this.command = command;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void action() {
        System.out.println("命令下达者：" + name);
        command.execute();
    }

}
