package com.jiaxw.pattern.creational.command;

/**
 * <p>命令接口</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public interface ICommand {

    /**
     * <p>执行，具体执行什么，让实现类去做吧</p>
     */
    void execute();

}
