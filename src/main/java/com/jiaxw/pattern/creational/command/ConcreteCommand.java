package com.jiaxw.pattern.creational.command;

/**
 * <p>具体命令类，实现命令接口，同时持有接收者，借接收者之手执行相应的命令</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class ConcreteCommand implements ICommand {

    private String command;

    /**
     * 当然，这里是一个接收者，实际对命令的发出者来说，他并不知道谁会接收命令并执行
     */
    private Receiver receiver;

    public ConcreteCommand(Receiver receiver, String command) {
        this.receiver = receiver;
        this.command = command;
    }

    @Override
    public void execute() {
        receiver.action(command);
    }
}
