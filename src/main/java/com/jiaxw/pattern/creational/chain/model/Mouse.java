package com.jiaxw.pattern.creational.chain.model;

/**
 * <p>鼠标</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class Mouse extends AbstractObject {

    @Override
    public void show() {
        System.out.println("鼠标");
    }

}
