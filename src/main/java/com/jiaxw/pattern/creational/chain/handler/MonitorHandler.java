package com.jiaxw.pattern.creational.chain.handler;

import com.jiaxw.pattern.creational.chain.AbstractHandler;
import com.jiaxw.pattern.creational.chain.filter.ComputerFilter;
import com.jiaxw.pattern.creational.chain.model.Computer;
import com.jiaxw.pattern.creational.chain.model.Monitor;

import java.util.List;

/**
 * <p>显示器处理事件</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class MonitorHandler extends AbstractHandler<List<Computer>, ComputerFilter> {

    @Override
    public List<Computer> filter(ComputerFilter filter) {
        List<Computer> computers = this.getData();
        Boolean loadMonitor = filter.getLoadMonitor();
        if (loadMonitor) {
            for (Computer computer : computers) {
                Monitor monitor = new Monitor();
                monitor.setId(idsMaker.nextId());
                monitor.setSize(23);
                monitor.setName("三星显示器");
                computer.setMonitor(monitor);
            }
        }
        return computers;
    }

}
