package com.jiaxw.pattern.creational.chain.model;

import lombok.Data;

/**
 * <p>键盘</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
@Data
public class KeyBord extends AbstractObject {

    /**
     * 键盘类型
     */
    private String type;

    public KeyBord(String type) {
        this.type = type;
    }

    @Override
    public void show() {
        System.out.println("键盘");
    }

}
