package com.jiaxw.pattern.creational.chain.filter;

import lombok.Data;

/**
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
@Data
public class ComputerFilter extends AbstractFilter {

    /**
     * 是否加载显示器
     */
    private Boolean loadMonitor = false;

    /**
     * 是否加载键盘
     */
    private Boolean loadKeyBord = false;

    /**
     * 是否加载鼠标
     */
    private Boolean loadMouse = false;

    public ComputerFilter() {

    }

}
