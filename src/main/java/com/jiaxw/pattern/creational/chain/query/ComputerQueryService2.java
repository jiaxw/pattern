package com.jiaxw.pattern.creational.chain.query;


import com.jiaxw.pattern.creational.chain.AbstractHandler;
import com.jiaxw.pattern.creational.chain.filter.ComputerFilter;
import com.jiaxw.pattern.creational.chain.model.Computer;
import com.jiaxw.pattern.creational.chain.handler.ComputerHandler;
import com.jiaxw.pattern.creational.chain.handler.KeyBordHandler;
import com.jiaxw.pattern.creational.chain.handler.MonitorHandler;
import com.jiaxw.pattern.creational.chain.handler.MouseHandler;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>使用了任务链的service业务层，实现根据过滤器查询电脑信息</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
@Service
public class ComputerQueryService2 {

    public List<Computer> query(ComputerFilter filter) {

        // 逐个创建任务
        ComputerHandler computerHandler = new ComputerHandler();
        MonitorHandler monitorHandler = new MonitorHandler();
        KeyBordHandler keyBordHandler = new KeyBordHandler();
        MouseHandler mouseHandler = new MouseHandler();

        // 设置任务之间的关系 == 设置任务之间的链条
        computerHandler.setNextHandler(monitorHandler);
        monitorHandler.setNextHandler(keyBordHandler);
        keyBordHandler.setNextHandler(mouseHandler);

        //执行任务链，从第一个任务事件开始执行，最终执行完拿到结果
        List<Computer> computers = (List<Computer>) AbstractHandler.process(computerHandler, filter);
        return computers;
    }

}
