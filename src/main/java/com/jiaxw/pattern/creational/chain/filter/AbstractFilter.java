package com.jiaxw.pattern.creational.chain.filter;

import lombok.Data;

import java.util.HashSet;

/**
 * <p>抽象过滤器</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
@Data
public abstract class AbstractFilter {

    /**
     * 根据ids查询
     */
    private HashSet<Long> ids = new HashSet<>();

    /**
     * 根据names查询
     */
    private HashSet<String> names = new HashSet<>();

    private Integer pageNum = 1;

    private Integer pageSize = 10;

    public void addIds(Long id) {
        this.ids.add(id);
    }

}
