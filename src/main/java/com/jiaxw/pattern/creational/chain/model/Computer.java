package com.jiaxw.pattern.creational.chain.model;

import lombok.Data;

/**
 * <p>电脑【组装电脑】</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
@Data
public class Computer extends AbstractObject {

    /**
     * 公司 -- 如联想、华硕、苹果...etc
     */
    private String company;

    /**
     * 价钱
     */
    private Double price;

    /**
     * 组成 -- 显示器
     */
    private Monitor monitor;

    /**
     * 组成 -- 键盘
     */
    private KeyBord keyBord;

    /**
     * 组成 -- 鼠标
     */
    private Mouse mouse;

    public Computer(String company, Double price) {
        this.company = company;
        this.price = price;
    }

    @Override
    public void show() {
        System.out.println("电脑");
    }

}
