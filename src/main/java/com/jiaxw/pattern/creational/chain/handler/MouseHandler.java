package com.jiaxw.pattern.creational.chain.handler;

import com.jiaxw.pattern.creational.chain.AbstractHandler;
import com.jiaxw.pattern.creational.chain.filter.ComputerFilter;
import com.jiaxw.pattern.creational.chain.model.Computer;
import com.jiaxw.pattern.creational.chain.model.Mouse;

import java.util.List;

/**
 * <p>鼠标处理事件</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class MouseHandler extends AbstractHandler<List<Computer>, ComputerFilter> {

    @Override
    public List<Computer> filter(ComputerFilter filter) {
        List<Computer> computers = this.getData();
        Boolean loadMouse = filter.getLoadMouse();
        if (loadMouse) {
            for (Computer computer : computers) {
                Mouse mouse = new Mouse();
                mouse.setId(idsMaker.nextId());
                mouse.setName("罗技鼠标");
                computer.setMouse(mouse);
            }
        }
        return computers;
    }

}
