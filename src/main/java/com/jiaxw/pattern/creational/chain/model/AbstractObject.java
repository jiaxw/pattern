package com.jiaxw.pattern.creational.chain.model;

import lombok.Data;

/**
 * <p>抽象对象类</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
@Data
public abstract class AbstractObject {

    private Long id;

    private String name;

    public abstract void show();

}
