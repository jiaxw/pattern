package com.jiaxw.pattern.creational.chain.model;

import lombok.Data;

/**
 * <p>显示器</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
@Data
public class Monitor extends AbstractObject {

    /**
     * 尺寸
     */
    private Integer size;

    public Monitor() {

    }

    @Override
    public void show() {
        System.out.println("显示器");
    }
}
