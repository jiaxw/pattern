package com.jiaxw.pattern.creational.chain.handler;

import com.jiaxw.pattern.creational.chain.AbstractHandler;
import com.jiaxw.pattern.creational.chain.filter.ComputerFilter;
import com.jiaxw.pattern.creational.chain.model.Computer;
import com.jiaxw.pattern.creational.chain.model.KeyBord;

import java.util.List;

/**
 * <p>键盘处理事件</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public class KeyBordHandler extends AbstractHandler<List<Computer>, ComputerFilter> {

    @Override
    public List<Computer> filter(ComputerFilter filter) {
        List<Computer> computers = this.getData();
        Boolean loadKeyBord = filter.getLoadKeyBord();
        if(loadKeyBord){
            for (Computer computer : computers) {
                KeyBord keyBord = new KeyBord("无线");
                keyBord.setId(idsMaker.nextId());
                keyBord.setName("雷柏键盘");
                computer.setKeyBord(keyBord);
            }
        }
        return  computers;
    }

}
