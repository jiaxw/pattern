package com.jiaxw.pattern.creational.iterator;

/**
 * <p>迭代器接口</p>
 *
 * @author jiaxw
 * @date 2020/11/13 14:52
 */
public interface IIterator {

    /**
     * <p>当前迭代器中是否还有元素</p>
     *
     * @return 有（true） 没有（false）
     */
    boolean hasNext();

    /**
     * <p>取出迭代器中的元素</p>
     *
     * @return 对象
     */
    Object next();

    /**
     * <p>取出迭代器中的第一个元素</p>
     *
     * @return 第一个元素
     */
    Object first();

}
